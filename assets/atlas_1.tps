<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.5.0</string>
        <key>fileName</key>
        <string>/Users/whilesocold/work/pashkov/catch-portrait/assets/atlas_1.tps</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>pixijs</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../static/images/atlas_{n}.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <true/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">CropKeepPos</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">images/cancel.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,13,28,27</rect>
                <key>scale9Paddings</key>
                <rect>14,13,28,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/creature_1.png</key>
            <key type="filename">images/creature_2.png</key>
            <key type="filename">images/creature_3.png</key>
            <key type="filename">images/creature_4.png</key>
            <key type="filename">images/creature_5.png</key>
            <key type="filename">images/creature_6.png</key>
            <key type="filename">images/creature_7.png</key>
            <key type="filename">images/creature_8.png</key>
            <key type="filename">images/creature_9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>150,150,300,300</rect>
                <key>scale9Paddings</key>
                <rect>150,150,300,300</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/frame_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>101,64,202,128</rect>
                <key>scale9Paddings</key>
                <rect>101,64,202,128</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/frame_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>108,80,217,160</rect>
                <key>scale9Paddings</key>
                <rect>108,80,217,160</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/ok.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,10,21,21</rect>
                <key>scale9Paddings</key>
                <rect>11,10,21,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/picture_1.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>160,113,320,225</rect>
                <key>scale9Paddings</key>
                <rect>160,113,320,225</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/picture_10.jpg</key>
            <key type="filename">images/picture_7.jpg</key>
            <key type="filename">images/picture_8.jpg</key>
            <key type="filename">images/picture_9.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>61,88,121,176</rect>
                <key>scale9Paddings</key>
                <rect>61,88,121,176</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/picture_2.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>146,120,293,240</rect>
                <key>scale9Paddings</key>
                <rect>146,120,293,240</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/picture_3.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>160,93,320,187</rect>
                <key>scale9Paddings</key>
                <rect>160,93,320,187</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/picture_4.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>88,120,176,240</rect>
                <key>scale9Paddings</key>
                <rect>88,120,176,240</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/picture_5.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>92,120,185,240</rect>
                <key>scale9Paddings</key>
                <rect>92,120,185,240</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/picture_6.jpg</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>87,120,175,240</rect>
                <key>scale9Paddings</key>
                <rect>87,120,175,240</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/score.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>60,23,121,46</rect>
                <key>scale9Paddings</key>
                <rect>60,23,121,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/timer_1.png</key>
            <key type="filename">images/timer_10.png</key>
            <key type="filename">images/timer_11.png</key>
            <key type="filename">images/timer_2.png</key>
            <key type="filename">images/timer_3.png</key>
            <key type="filename">images/timer_4.png</key>
            <key type="filename">images/timer_5.png</key>
            <key type="filename">images/timer_6.png</key>
            <key type="filename">images/timer_7.png</key>
            <key type="filename">images/timer_8.png</key>
            <key type="filename">images/timer_9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>128,21,256,41</rect>
                <key>scale9Paddings</key>
                <rect>128,21,256,41</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">images/window_failed.png</key>
            <key type="filename">images/window_success.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>279,206,557,413</rect>
                <key>scale9Paddings</key>
                <rect>279,206,557,413</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>images/cancel.png</filename>
            <filename>images/frame_1.png</filename>
            <filename>images/frame_2.png</filename>
            <filename>images/ok.png</filename>
            <filename>images/score.png</filename>
            <filename>images/timer_1.png</filename>
            <filename>images/timer_2.png</filename>
            <filename>images/timer_3.png</filename>
            <filename>images/timer_4.png</filename>
            <filename>images/timer_5.png</filename>
            <filename>images/timer_6.png</filename>
            <filename>images/timer_7.png</filename>
            <filename>images/timer_8.png</filename>
            <filename>images/timer_9.png</filename>
            <filename>images/timer_10.png</filename>
            <filename>images/timer_11.png</filename>
            <filename>images/creature_1.png</filename>
            <filename>images/creature_2.png</filename>
            <filename>images/creature_3.png</filename>
            <filename>images/creature_4.png</filename>
            <filename>images/creature_5.png</filename>
            <filename>images/creature_6.png</filename>
            <filename>images/creature_7.png</filename>
            <filename>images/creature_8.png</filename>
            <filename>images/creature_9.png</filename>
            <filename>images/window_success.png</filename>
            <filename>images/window_failed.png</filename>
            <filename>images/picture_1.jpg</filename>
            <filename>images/picture_2.jpg</filename>
            <filename>images/picture_3.jpg</filename>
            <filename>images/picture_4.jpg</filename>
            <filename>images/picture_5.jpg</filename>
            <filename>images/picture_6.jpg</filename>
            <filename>images/picture_7.jpg</filename>
            <filename>images/picture_8.jpg</filename>
            <filename>images/picture_9.jpg</filename>
            <filename>images/picture_10.jpg</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
