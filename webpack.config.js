require('dotenv').config('.env')

const path = require('path')
const webpack = require('webpack')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin')

module.exports = {
  entry: {
    bundle: './src/index',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.[hash].js',
  },
  optimization: {
    minimize: true,
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.BUILD_MODE': JSON.stringify(process.env.BUILD_MODE || 'development'),
    }),
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({
      template: './static/index.html',
      filename: 'index.html',
      publicPath: '/',
      inject: true,
      WEBSOCKET: process.env.WEBSOCKET,
      inlineSource: '.(js|css)$',
    }),
    new ScriptExtHtmlWebpackPlugin()
  ],
  resolve: {
    symlinks: false,
    extensions: [
      '.ts', '.js', '.json', '.png',
    ],
  },
  devServer: {
    disableHostCheck: true,
  },
  node: {
    fs: 'empty',
    dgram: 'empty',
    net: 'empty',
    tls: 'empty',
  },
  module: {
    rules: [{
      test: /\.(png|jpg|gif)$/i,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 8192,
          },
        },
      ],
    }, {
      test: /\.ts$/,
      loaders: ['babel-loader?plugins[]=transform-class-properties', 'awesome-typescript-loader'],
      include: path.join(__dirname, 'src'),
    }, {
      test: /\.hbs/,
      loader: 'handlebars-loader',
      exclude: /(node_modules|bower_components)/,
    }],
  },
}