import * as PIXI from 'pixi.js'

import { Resources } from '../../utils/Resources'

export class Score extends PIXI.Container {
  private sprite: PIXI.Sprite
  private label: PIXI.Text

  constructor() {
    super()

    this.sprite = new PIXI.Sprite(Resources.get('score.png'))
    this.sprite.anchor.set(0.5)
    this.addChild(this.sprite)

    this.label = new PIXI.Text('0')
    this.label.anchor.set(0.5)
    this.label.position.set(32, -7)
    this.label.style.align = 'center'
    this.label.style.fontFamily = 'Cartwheel'
    this.label.style.fontSize = 42
    this.addChild(this.label)
  }

  setScore(value: number) {
    this.label.text = value.toString()
  }
}