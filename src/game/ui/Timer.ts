import * as PIXI from 'pixi.js'

import { Resources } from '../../utils/Resources'
import { Utils } from '../core/Utils'

export class Timer extends PIXI.Container {
  private framesCount: number = 11

  private sprite: PIXI.Sprite
  private label: PIXI.Text

  constructor() {
    super()

    this.sprite = new PIXI.Sprite()
    this.sprite.anchor.set(0.5)
    this.addChild(this.sprite)

    this.label = new PIXI.Text('542')
    this.label.anchor.set(0.5)
    this.label.position.set(10, -7)
    this.label.style.align = 'center'
    this.label.style.fontFamily = 'Cartwheel'
    this.label.style.fontSize = 42
    this.addChild(this.label)

    this.setSeconds(0)
    this.setPercent(50)
  }

  setSeconds(value: number) {
    this.label.text = Utils.secondsToHHMMSS(value)
  }

  setPercent(percent: number) {
    const frame = Math.floor(Utils.remap(percent, 0, 100, 1, this.framesCount))

    this.sprite.texture = Resources.get('timer_' + frame + '.png')
  }
}