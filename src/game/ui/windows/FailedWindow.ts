import { BaseWindow } from './BaseWindow'

export class FailedWindow extends BaseWindow {
  constructor(text: string) {
    super('window_failed.png', text, [2, 6, 7])
  }
}