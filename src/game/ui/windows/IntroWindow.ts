import { BaseWindow } from './BaseWindow'

export class IntroWindow extends BaseWindow {
  constructor(text) {
    super('window_success.png', text, [5, 8, 9])
  }
}