import * as PIXI from 'pixi.js'

import { Resources } from '../../../utils/Resources'

export class BaseWindow extends PIXI.Container {
  private overlay: PIXI.Graphics
  private sprite: PIXI.Sprite
  private label: PIXI.Text

  private creature: PIXI.AnimatedSprite

  constructor(sprite: string, text: string, creatureFrames: any = [1]) {
    super()

    this.overlay = new PIXI.Graphics()
    this.overlay.interactive = true
    this.addChild(this.overlay)

    this.sprite = new PIXI.Sprite(Resources.get(sprite))
    this.sprite.anchor.set(0.5)
    this.addChild(this.sprite)

    this.label = new PIXI.Text(text)
    this.label.anchor.set(0, 0.5)
    this.label.position.set(-this.sprite.width / 2 + 60, 0)
    this.label.style.align = 'left'
    this.label.style.wordWrap = true
    this.label.style.fontFamily = 'Cartwheel'
    this.label.style.wordWrapWidth = this.sprite.width * 0.85
    this.label.style.fontSize = 32

    this.addChild(this.label)

    this.initCreature(creatureFrames)
  }

  initCreature(creatureFrames: any): void {
    const frames = []

    for (let i = 0; i < creatureFrames.length; i++) {
      frames.push(Resources.get('creature_' + creatureFrames[i] + '.png'))
    }

    for (let i = creatureFrames.length - 1; i >= 0; i--) {
      frames.push(Resources.get('creature_' + creatureFrames[i] + '.png'))
    }

    this.creature = new PIXI.AnimatedSprite(frames)
    this.creature.anchor.set(0.5, 0.5)
    this.creature.loop = false
    this.creature.animationSpeed = 0.15
    this.addChild(this.creature)

    setTimeout(() => {
      this.creature.gotoAndPlay(0)
    }, 1000)
  }

  resize(width: number, height: number): void {
    const leftTopSide = this.toLocal(new PIXI.Point(0, 0))
    const rightBottomSide = this.toLocal(new PIXI.Point(width * 2, height * 2))
    const centerBottomSide = this.toLocal(new PIXI.Point(width, height))

    const x = leftTopSide.x
    const y = leftTopSide.y
    const w = rightBottomSide.x
    const h = rightBottomSide.y

    this.overlay.clear()
    this.overlay.beginFill(0x000000, 0.85)
    this.overlay.drawRect(x, y, w, h)
    this.overlay.endFill()


    this.creature.scale.set(width > height ? 0.5 : 0.7)
    this.creature.position.set(centerBottomSide.x - this.creature.width / 2, centerBottomSide.y - this.creature.height / 2)
  }

  getLocalBounds(): PIXI.Rectangle {
    return this.sprite.getLocalBounds()
  }
}