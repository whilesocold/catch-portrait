import { BaseWindow } from './BaseWindow'

export class SuccessWindow extends BaseWindow {
  constructor(text) {
    super('window_success.png', text, [4])
  }
}