import * as PIXI from 'pixi.js'

export enum StateKind {
  COMPLETE = 'StateKind.COMPLETE',
  LEVEL = 'StateKind.LEVEL'
}

export class State extends PIXI.Container {
  private stateType: StateKind

  constructor(stateType: StateKind) {
    super()

    this.stateType = stateType
  }

  init(props: any): void {
  }

  release(): void {
    this.removeAllListeners()

    while (this.children.length > 0) {
      this.removeChildAt(0)
    }
  }

  resize(width: number, height: number): void {
  }

  update(dt: number): void {

  }

  getType(): StateKind {
    return this.stateType
  }
}