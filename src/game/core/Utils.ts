import * as PIXI from 'pixi.js'

export class Utils {
  static loadAtlasFromJson(name: string, image: HTMLImageElement, json: any, resolution: number = parseInt(json.meta.scale, 10)): void {
    const frames = json.frames
    const frameKeys = Object.keys(frames)

    let frameIndex = 0

    while (frameIndex < frameKeys.length) {
      const frame = frames[frameKeys[frameIndex]]
      const rect = frame.frame

      if (rect) {
        let size = null
        let trim = null

        if (frame.rotated) {
          size = new PIXI.Rectangle(rect.x, rect.y, rect.h, rect.w)
        } else {
          size = new PIXI.Rectangle(rect.x, rect.y, rect.w, rect.h)
        }

        //  Check to see if the sprite is trimmed
        if (frame.trimmed) {
          trim = new PIXI.Rectangle(
            frame.spriteSourceSize.x / resolution,
            frame.spriteSourceSize.y / resolution,
            frame.sourceSize.w / resolution,
            frame.sourceSize.h / resolution,
          )
        }

        // flip the width and height!
        if (frame.rotated) {
          const temp = size.width
          size.width = size.height
          size.height = temp
        }

        size.x /= resolution
        size.y /= resolution
        size.width /= resolution
        size.height /= resolution

        PIXI.utils.TextureCache[frameKeys[frameIndex]] = new PIXI.Texture(PIXI.BaseTexture.from(image), size, size.clone(), trim, frame.rotated)
      }
      frameIndex++
    }
  }

  static remap(value: number, low1: number, high1: number, low2: number, high2: number): number {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1)
  }

  static secondsToHHMMSS(seconds:number):string {
    const date = new Date(null)

    date.setSeconds(seconds)

    return date.toISOString().substr(11, 8)
  }
}