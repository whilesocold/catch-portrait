import * as PIXI from 'pixi.js'
import { State, StateKind } from './State'

export class StateManager {
  private parent: PIXI.Container
  private container: PIXI.Container

  private states: Map<string, State> = new Map<string, State>()
  private currentType: StateKind

  constructor(parent: PIXI.Container) {
    this.parent = parent

    this.container = new PIXI.Container()
    this.parent.addChild(this.container)
  }

  addState(type: StateKind, classType: any, props: any = {}): void {
    this.states.set(type, new classType(type, props))
  }

  setState(type: StateKind, props: any = {}): State {
    if (this.getState()) {
      this.resetState()
    }

    const state = this.getState(type)

    if (state) {
      state.init(props)

      this.container.addChild(state)
      this.currentType = type

      return state
    }

    return null
  }

  resetState(): void {
    const state = this.getState()

    if (state) {
      state.release()
      this.container.removeChild(state)
    }
  }

  getState(type: StateKind = null): State {
    if (this.states.has(type)) {
      return this.states.get(type)
    }

    return this.states.get(this.currentType)
  }

  resize(width: number, height: number): void {
    /*
    const bounds = this.container.getLocalBounds()
    const scale = Math.min(1, Math.min(width / bounds.width, height / bounds.height))

    this.container.scale.set(scale)
    this.container.position.set(-bounds.width * scale / 2, -bounds.height * scale / 2)
     */

    const state = this.getState()

    if (state) {
      state.resize(width, height)
    }
  }

  update(dt: number): void {
    const state = this.getState()

    if (state) {
      state.update(dt)
    }
  }
}