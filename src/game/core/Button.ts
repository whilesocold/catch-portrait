import * as PIXI from 'pixi.js'
import { Resources } from '../../utils/Resources'

export class Button extends PIXI.Container {
  private sprite: PIXI.Sprite
  private label: PIXI.Text

  constructor(style: any = {
    sprite: null,
    text: '',
    fontFill: 0xffffff,
    fontSize: 32,
  }) {
    super()

    this.interactive = true
    this.buttonMode = true

    this.sprite = new PIXI.Sprite(Resources.get(style.sprite))
    this.sprite.anchor.set(0.5)

    this.label = new PIXI.Text(style.text)
    this.label.anchor.set(0.5)
    this.label.position.set(0, -5)
    this.label.style.align = 'center'
    this.label.style.fontFamily = 'Cartwheel'
    this.label.style.fill = style.fontFill || 0x000000
    this.label.style.fontSize = style.fontSize || 18

    this.addChild(this.sprite)
    this.addChild(this.label)
  }

  setText(value: string) {
    this.label.text = value
  }
}