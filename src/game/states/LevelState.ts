import * as PIXI from 'pixi.js'

import { Linear, TweenMax } from 'gsap'
import * as _ from 'underscore'

import { State } from '../core/State'
import { Resources } from '../../utils/Resources'
import { Score } from '../ui/Score'
import { Timer } from '../ui/Timer'
import { Utils } from '../core/Utils'
import { Button } from '../core/Button'
import { BaseWindow } from '../ui/windows/BaseWindow'

import config from '../../config'
import { FailedWindow } from '../ui/windows/FailedWindow'
import { SuccessWindow } from '../ui/windows/SuccessWindow'
import { IntroWindow } from '../ui/windows/IntroWindow'

export class Picture extends PIXI.Container {
  private sprite: PIXI.Sprite
  private frame: PIXI.Sprite

  public stillLife: boolean

  constructor(sprite: string, stillLife: boolean) {
    super()

    this.stillLife = stillLife

    this.sprite = new PIXI.Sprite(Resources.get(sprite))
    this.sprite.anchor.set(0.5)

    const landscape = this.sprite.width >= this.sprite.height

    this.frame = new PIXI.Sprite(Resources.get(landscape ? 'frame_2.png' : 'frame_1.png'))
    this.frame.anchor.set(0.5)

    /*
    if (landscape) {
      this.sprite.width = this.frame.width * 0.8
      this.sprite.height = this.frame.height * 0.7
      this.sprite.position.set(landscape ? -5 : 0, 0)

    } else {
      this.sprite.width = this.frame.width * 0.8
      this.sprite.height = this.frame.height * 0.9
      this.sprite.position.set(landscape ? -5 : 0, 0)
    }
     */

    this.addChild(this.frame)
    this.addChild(this.sprite)
  }
}

export class LevelState extends State {
  private ratio: number = 1

  private backgroundCount: number = 5
  private backgroundKind: number
  private background: PIXI.Sprite

  private score: Score
  private timer: Timer
  private okButton: Button
  private cancelButton: Button

  private creature: PIXI.AnimatedSprite
  private picture: Picture

  private pictures: any
  private pictureIndex: number

  private isComplete: boolean
  private isPictureSelected: boolean

  private currentScore: number
  private scoreAdding: number = 10

  private pictureContainer: PIXI.Container
  private creatureContainer: PIXI.Container
  private windowContainer: PIXI.Container

  private timerId: any = -1
  private currentTime: number = 0
  private totalTime: number = 60

  init(props: any): void {
    super.init(props)

    this.backgroundKind = _.random(1, this.backgroundCount)
    this.background = new PIXI.Sprite(Resources.get('bg_' + this.backgroundKind + '_jpg'))
    this.background.anchor.set(0.5)
    this.addChild(this.background)

    this.pictureContainer = new PIXI.Container()
    this.addChild(this.pictureContainer)

    this.score = new Score()
    this.addChild(this.score)

    this.timer = new Timer()
    this.addChild(this.timer)

    this.okButton = new Button({ sprite: 'ok.png', text: config.okText, fontSize: 42, fontFill: 0xffffff })
    this.okButton.on('pointerdown', () => {
      this.selectPicture(true)
      this.animateButtonClick(this.okButton)
    })
    this.addChild(this.okButton)

    this.cancelButton = new Button({ sprite: 'cancel.png', text: config.cancelText, fontSize: 42, fontFill: 0xffffff })
    this.cancelButton.on('pointerdown', () => {
      this.selectPicture(false)
      this.animateButtonClick(this.cancelButton)
    })
    this.addChild(this.cancelButton)

    this.creatureContainer = new PIXI.Container()
    this.addChild(this.creatureContainer)

    this.windowContainer = new PIXI.Container()
    this.addChild(this.windowContainer)

    this.animateCreature()
  }

  animateCreature(creatureFrames: any = [1]): void {
    const frames = []

    for (let i = 0; i < creatureFrames.length; i++) {
      frames.push(Resources.get('creature_' + creatureFrames[i] + '.png'))
    }

    for (let i = creatureFrames.length - 1; i >= 0; i--) {
      frames.push(Resources.get('creature_' + creatureFrames[i] + '.png'))
    }

    if (this.creature) {
      this.creatureContainer.removeChild(this.creature)
    }

    this.creature = new PIXI.AnimatedSprite(frames)
    this.creature.anchor.set(0.5, 0.5)
    this.creature.loop = false
    this.creature.gotoAndPlay(0)
    this.creature.animationSpeed = 0.15
    this.creatureContainer.addChild(this.creature)

    window['app'].resize()
  }

  animateButtonClick(object): void {
    TweenMax.killTweensOf(object)
    TweenMax.to(object.scale, 0.2, {
      x: object.initialScale.x * 1.4, y: object.initialScale.y * 1.4, onComplete: () => {
        TweenMax.to(object.scale, 0.2, { x: object.initialScale.x, y: object.initialScale.y })
      },
    })
  }

  showWindow(classType: any, text: string): BaseWindow {
    const window = new classType(text)

    window.alpha = 0

    TweenMax.to(window, 0.5, { alpha: 1 })

    this.windowContainer.addChild(window)

    return window
  }

  hideWindow(window: BaseWindow): Promise<void> {
    return new Promise(resolve => {
      TweenMax.killTweensOf(window)
      TweenMax.to(window, 0.5, {
        alpha: 0, onComplete: () => {
          this.windowContainer.removeChild(window)

          resolve()
        },
      })
    })
  }

  start(): void {
    this.stop()
    this.updateTime()

    const intoWindow = this.showWindow(IntroWindow, config.introText)

    intoWindow.interactive = true
    intoWindow.buttonMode = true
    intoWindow.once('pointerdown', async () => {
      await this.hideWindow(intoWindow)

      this.timerId = setInterval(() => {
        if (this.updateTime()) {
          this.onTimerComplete()
        }
      }, 1000)

      this.currentScore = 0

      this.pictures = _.shuffle(config.pictures)
      this.pictureIndex = 0

      setTimeout(() => {
        this.pushPicture(this.pictureIndex)
        this.repositionPicture()

      }, 300)
    })
  }

  stop(): void {
    if (this.timerId > -1) {
      this.timerId = 0
      clearInterval(this.timerId)
    }

    this.currentTime = 0
    this.totalTime = config.levelTime
    this.isComplete = false

    TweenMax.killAll()
  }

  updateTime(): boolean {
    const percent = Math.floor(Utils.remap(++this.currentTime, 0, this.totalTime, 100, 0))
    const seconds = this.totalTime - this.currentTime

    this.timer.setSeconds(seconds)
    this.timer.setPercent(percent)

    return seconds === 0
  }

  hasPicture(): boolean {
    return this.pictureIndex < this.pictures.length - 1
  }

  pushPicture(index: number): void {
    console.log('push')

    const pictureData = this.pictures[index]

    this.picture = new Picture(pictureData.sprite, pictureData.stillLife)
    this.picture.scale.set(1.5)

    this.pictureContainer.addChild(this.picture)

    this.isPictureSelected = false
  }

  checkComplete(): void {
    console.log('check')

    this.pictureContainer.removeChild(this.picture)
    this.picture = null

    if (!this.isComplete && this.hasPicture()) {
      setTimeout(() => {
        this.pushPicture(++this.pictureIndex)
        this.repositionPicture()

      }, 300)

    } else {
      if (!this.isComplete) {
        this.isComplete = true
        this.picture = null

        this.stop()

        const success = this.currentScore > 0

        if (success) {
          this.showWindow(SuccessWindow, config.successText)

        } else {
          this.showWindow(FailedWindow, config.failedText)
        }
      }
    }

    const app = window['app']

    app.resize(app.currWidth, app.currHeight)
  }

  repositionPicture(): void {
    if (this.picture && !this.isComplete) {
      const app = window['app']
      const appContainer = app.containerCenter

      TweenMax.killTweensOf(this.picture)

      const onComplete = () => this.checkComplete()

      if (app.currWidth > app.currHeight) {
        const start = this.pictureContainer.toLocal(new PIXI.Point(-app.currWidth / 4, 0), appContainer)
        const end = this.pictureContainer.toLocal(new PIXI.Point(app.currWidth / 4, 0), appContainer)

        this.picture.position.set(start.x - this.picture.width / 2, start.y)

        TweenMax.to(this.picture, 6, { x: end.x + this.picture.width / 2, ease: Linear.easeNone, onComplete: () => onComplete() })

      } else {
        const start = this.pictureContainer.toLocal(new PIXI.Point(0, -app.currHeight / 4), appContainer)
        const end = this.pictureContainer.toLocal(new PIXI.Point(0, app.currHeight / 4), appContainer)

        this.picture.position.set(start.x, start.y - this.picture.height / 2)

        TweenMax.to(this.picture, 6, { y: end.y + this.picture.height / 2, ease: Linear.easeNone, onComplete: () => onComplete() })
      }
    }
  }

  addScore(value: number): void {
    this.currentScore += value
    this.score.setScore(this.currentScore)
  }

  selectPicture(stillLife: boolean): void {
    if (this.picture && !this.isPictureSelected) {
      const success = stillLife && this.picture.stillLife || !stillLife && !this.picture.stillLife

      TweenMax.killTweensOf(this.picture)

      this.isPictureSelected = true

      if (success) {
        this.addScore(this.scoreAdding)
        this.animateCreature([1, 2, 6, 7])

        TweenMax.to(this.picture.scale, 0.3, {
          x: 4, y: 4,
        })

        TweenMax.to(this.picture, 0.3, {
          alpha: 0, onComplete: () => {
            this.checkComplete()
          },
        })

      } else {
        this.animateCreature([1, 5, 8, 9])

        TweenMax.to(this.picture, 0.3, {
          alpha: 0, onComplete: () => {
            this.checkComplete()
          },
        })
      }
    }
  }

  release(): void {
    super.release()
  }

  resize(width: number = -1, height: number = -1): void {
    const backgroundBounds = this.background.getLocalBounds()
    const ratio = Math.max(width / backgroundBounds.width, height / backgroundBounds.height)

    const landscape = width > height

    this.ratio = ratio
    this.background.scale.set(ratio)

    this.windowContainer.children.forEach(children => {
      const window = (children as BaseWindow)
      const windowBounds = window.getLocalBounds()
      const windowRatio = Math.min(width / windowBounds.width, height / windowBounds.height)

      window.scale.set(Math.min(0.6, windowRatio))
      window.resize(width, height)
    })

    if (landscape) {
      this.pictureContainer.scale.set(ratio)

      const leftTopSide = this.toLocal(new PIXI.Point(0, 0))
      const scoreMargin = new PIXI.Point(20 * ratio, 20 * ratio)

      this.score.scale.set(ratio)
      this.score.position.set(
        leftTopSide.x + this.score.width / 2 + scoreMargin.x,
        leftTopSide.y + this.score.height / 2 + scoreMargin.y)

      const rightTopSide = this.toLocal(new PIXI.Point(width, 0))
      const timerMargin = new PIXI.Point(-20 * ratio, 20 * ratio)

      this.timer.scale.set(ratio)
      this.timer.position.set(
        rightTopSide.x - this.timer.width / 2 + timerMargin.x,
        rightTopSide.y + this.timer.height / 2 + timerMargin.y)

      const centerBottomSide = this.toLocal(new PIXI.Point(width / 2, height))
      const buttonMargin = new PIXI.Point(40 * ratio, 30 * ratio)

      this.okButton.scale.set(ratio)
      this.okButton.position.set(
        centerBottomSide.x - this.okButton.width / 2 - buttonMargin.x,
        centerBottomSide.y - this.okButton.height / 2 - buttonMargin.y)

      this.cancelButton.scale.set(ratio)
      this.cancelButton.position.set(
        centerBottomSide.x + this.cancelButton.width / 2 + buttonMargin.x,
        centerBottomSide.y - this.cancelButton.height / 2 - buttonMargin.y)

      const rightBottomSide = this.toLocal(new PIXI.Point(width, height))
      const creatureMargin = new PIXI.Point(-20 * ratio, 0)

      this.creature.scale.set(ratio * 0.55)
      this.creature.position.set(
        rightBottomSide.x - this.creature.width / 2 + creatureMargin.x,
        rightBottomSide.y - this.creature.height / 2 + creatureMargin.y)

    } else {
      this.pictureContainer.scale.set(ratio * 0.5)

      const leftTopSide = this.toLocal(new PIXI.Point(0, 0))
      const scoreMargin = new PIXI.Point(5 * ratio, 20 * ratio)

      this.score.scale.set(ratio)
      this.score.position.set(
        leftTopSide.x + this.score.width / 2 + scoreMargin.x,
        leftTopSide.y + this.score.height / 2 + scoreMargin.y)

      const rightTopSide = this.toLocal(new PIXI.Point(width, 0))
      const timerMargin = new PIXI.Point(-5 * ratio, 10 * ratio)

      this.timer.scale.set(ratio * 0.65)
      this.timer.position.set(
        rightTopSide.x - this.timer.width / 2 + timerMargin.x,
        rightTopSide.y + this.timer.height + timerMargin.y)

      const centerBottomSide = this.toLocal(new PIXI.Point(width / 2, height))
      const buttonMargin = new PIXI.Point(40 * ratio, 200 * ratio)

      this.okButton.scale.set(ratio * 0.6)
      this.okButton.position.set(
        centerBottomSide.x - this.okButton.width / 2 - buttonMargin.x,
        centerBottomSide.y - this.timer.height / 2 - this.okButton.height / 2 - buttonMargin.y)

      this.cancelButton.scale.set(ratio * 0.6)
      this.cancelButton.position.set(
        centerBottomSide.x + this.cancelButton.width / 2 + buttonMargin.x,
        centerBottomSide.y - this.timer.height / 2 - this.cancelButton.height / 2 - buttonMargin.y)

      this.creature.scale.set(ratio * 0.5)
      this.creature.position.set(
        centerBottomSide.x,
        centerBottomSide.y - this.creature.height / 2)
    }

    this.okButton['initialScale'] = this.okButton.scale.clone()
    this.cancelButton['initialScale'] = this.cancelButton.scale.clone()
  }

  onTimerComplete(): void {
    this.stop()
  }
}